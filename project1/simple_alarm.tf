module "test_basic_alarm" {
  source              = "../modules/alarm"
  name                = "basic-alarm-example"
  description         = <<EOF
  Alarm example
  Description with multiple lines
  EOF
  metric_namespace    = "AWS/Lambda"
  metric_name         = "IteratorAge"
  metric_dimensions   = { "FunctionName" = "example-function" }
  metric_statistic    = "Maximum"
  metric_period       = 300
  comparison_operator = "GreaterThanThreshold"
  threshold           = 10
  datapoints_to_alarm = 1
  evaluation_periods  = 1
  treat_missing_data  = "missing"
  notification_agents = var.action_notification_sns
  tags                = {}
}
