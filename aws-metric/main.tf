module "cloudwatch_metric-alarm" {
  source = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "1.2.0"
  alarm_name = "terraform-metric-alarm"
  alarm_description = "alarm built following: https://registry.terraform.io/modules/terraform-aws-modules/cloudwatch/aws/latest/submodules/metric-alarm?tab=inputs "
  metric_name = "IteratorAge"
  namespace = "AWS/Lambda"
  dimensions = {
    "FunctionName" = "example-function"
  }
  period = 60
  statistic = "Maximum"
  comparison_operator = "GreaterThanThreshold"
  threshold = 10
  evaluation_periods = 1
  datapoints_to_alarm = 1
  treat_missing_data = "missing"
  tags = {
    type = "test"
  }
}