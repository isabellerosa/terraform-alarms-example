provider "aws" {}

terraform {
  required_version = ">= 0.13.0"

  backend "s3" {
    encrypt = true
  }
}

module "fix_name" {
  source = "./project1"
}

module "second_project" {
  source = "./project2"
}

module "aws_metric" {
  source = "./aws-metric"
}