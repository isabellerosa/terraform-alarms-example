locals {
  notification_agents = var.notification_agents == "" ? null : split(",", var.notification_agents)
}

resource "aws_cloudwatch_metric_alarm" "alarm" {
  alarm_name                = var.name
  alarm_description         = var.description
  namespace                 = var.metric_namespace
  metric_name               = var.metric_name
  dimensions                = var.metric_dimensions
  statistic                 = var.metric_statistic
  period                    = var.metric_period
  comparison_operator       = var.comparison_operator
  threshold                 = var.threshold
  datapoints_to_alarm       = var.datapoints_to_alarm
  evaluation_periods        = var.evaluation_periods
  treat_missing_data        = var.treat_missing_data
  alarm_actions             = local.notification_agents
  ok_actions                = local.notification_agents
  insufficient_data_actions = []
  tags                      = var.tags
  actions_enabled           = false

  lifecycle {
    create_before_destroy = true
  }
}
