locals {
  notification_agents = var.notification_agents == "" ? null : split(",", var.notification_agents)
  metric_statistic    = "Sum"
}

resource "aws_cloudwatch_metric_alarm" "availability_alarm" {
  alarm_name                = var.name
  alarm_description         = var.description
  comparison_operator       = var.comparison_operator
  threshold                 = var.threshold
  datapoints_to_alarm       = var.datapoints_to_alarm
  evaluation_periods        = var.evaluation_periods
  treat_missing_data        = var.treat_missing_data
  alarm_actions             = local.notification_agents
  ok_actions                = local.notification_agents
  insufficient_data_actions = []
  tags                      = var.tags
  actions_enabled           = false


  metric_query {
    id          = "errors"
    return_data = false

    metric {
      namespace   = var.metric_namespace
      metric_name = "Errors"
      dimensions  = var.metric_dimensions
      stat        = local.metric_statistic
      period      = var.metric_period
    }
  }

  metric_query {
    id          = "invocations"
    return_data = false

    metric {
      namespace   = var.metric_namespace
      metric_name = "Invocations"
      dimensions  = var.metric_dimensions
      stat        = local.metric_statistic
      period      = var.metric_period
    }
  }

  metric_query {
    id          = "availability"
    return_data = true
    label       = "Availability"
    expression  = "100 - 100 * errors / invocations"
  }
}
