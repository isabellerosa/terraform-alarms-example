# Alarm

variable name {
  description = "Alarm name"
  type        = string
}

variable description {
  description = "Alarm description"
  type        = string
}

variable tags {
  description = "Alarm tags"
  type        = map(string)
}

variable notification_agents {
  description = "Resources to notify on alarm actions"
  type        = string
}


# Condition

variable comparison_operator {
  description = "Arithmetic operation used to compare statistic to threshold"
  type        = string
}

variable threshold {
  description = "Value against which the statistic will be compared to"
  type        = number
}

variable datapoints_to_alarm {
  description = "How many times the error condition must occur to trigger the alarm"
  type        = number
}

variable evaluation_periods {
  description = "The number of periods/datapoints used to evaluate the alarm state"
  type        = number
}

variable treat_missing_data {
  description = "How to treat missing data when evaluating the alarm"
  type        = string
}

# Metric

variable metric_namespace {
  description = "Alarm's service namespace"
  type        = string
}

variable metric_dimensions {
  description = "Map filtering and sorting resources to use on metrics"
  type        = map(string)
}

variable metric_statistic {
  description = "The statistic operation to apply on metric"
  type        = string
}

variable metric_period {
  description = "Period in seconds when the statistic will be applied, generating a datapoint"
  type        = number
}
